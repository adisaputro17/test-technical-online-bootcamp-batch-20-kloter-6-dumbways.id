#include <stdio.h>

void cariNilaiTeratas(int n,int data[]){
    int tmp;
    
    for(int i=0;i<n;i++){
        for (int j=i+1;j<n;j++){
            if (data[j] > data[i]){
                tmp = data[i];
                data[i] = data[j];
                data[j] = tmp;
            }
        }
    }
    
    tmp = 0;
    for(int i=0;i<n;i++){
        if(tmp > 3){
            break;
        }
        if(data[i]%2 > 0){
            tmp += 1;
            printf("Nilai tertinggi %d = %d\n",tmp,data[i]);
        }
    }
}

int main()
{
    int n,data[100];
    printf("Masukkan jumlah data : ");
    scanf("%d",&n);
    
    for (int i=0;i<n;i++){
        printf("Nilai data ke %d = ",i);
        scanf("%d",&data[i]);
    }
    
    printf("----------------------\n");
    
    cariNilaiTeratas(n,data);

    return 0;
}
