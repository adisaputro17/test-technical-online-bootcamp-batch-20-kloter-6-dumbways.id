#include <stdio.h>

void hitungUangkembalian(int beli, int bayar)
{
    int kembali, sisa, jumlah;
    kembali = bayar - beli;
    sisa = kembali;
    
    if(kembali < 0){
        printf("Uang yang anda bayarkan kurang sebesar Rp. %d", -1 * kembali);
    }else if(kembali == 0){
        printf("Uang yang anda bayarkan pas");
    }else{
        printf("Jumlah uang kembali anda Rp. %d", kembali);
        printf("\n");
        printf("Pecahan uang kembali : \n");
        
        if(sisa >= 50000){
            jumlah = sisa / 50000;
            printf("Uang pecahan Rp. 50000 sebanyak %d lembar\n", jumlah);
            sisa = sisa - (jumlah * 50000);
        }
        if(sisa >= 20000){
            jumlah = sisa / 20000;
            printf("Uang pecahan Rp. 20000 sebanyak %d lembar\n", jumlah);
            sisa = sisa - (jumlah * 20000);
        }
        if(sisa >= 10000){
            jumlah = sisa / 10000;
            printf("Uang pecahan Rp. 10000 sebanyak %d lembar\n", jumlah);
            sisa = sisa - (jumlah * 10000);
        }
        if(sisa >= 5000){
            jumlah = sisa / 5000;
            printf("Uang pecahan Rp. 5000 sebanyak %d lembar\n", jumlah);
            sisa = sisa - (jumlah * 5000);
        }
        if(sisa >= 2000){
            jumlah = sisa / 2000;
            printf("Uang pecahan Rp. 2000 sebanyak %d lembar\n", jumlah);
            sisa = sisa - (jumlah * 2000);
        }
        if(sisa >= 1000){
            jumlah = sisa / 1000;
            printf("Uang pecahan Rp. 1000 sebanyak %d lembar\n", jumlah);
            sisa = sisa - (jumlah * 1000);
        }
        if(sisa >= 500){
            jumlah = sisa / 500;
            printf("Uang pecahan Rp. 500 sebanyak %d lembar\n", jumlah);
            sisa = sisa - (jumlah * 500);
        }
        if(sisa >= 100){
            jumlah = sisa / 100;
            printf("Uang pecahan Rp. 100 sebanyak %d lembar\n", jumlah);
            sisa = sisa - (jumlah * 100);
        }
    }
}

int main()
{
    int beli, bayar;
    
    printf("Total bayar = Rp. ");
    scanf("%d",&beli);
    
    printf("Uang bayar = Rp. ");
    scanf("%d",&bayar);
    
    hitungUangkembalian(beli,bayar);
    
    return 0;
}
