<!DOCTYPE html>
<html lang="en">
<head>
	<title>Mobile Ganggu</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow-sm">
		<a class="navbar-brand" href="index.php" style="font-family: Lobster;">Mobile Ganggu</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav mr-auto">

			</div>
			<div class="navbar-nav">

				<button type="button" class="btn btn-light" data-toggle="modal" data-target="#add_role">Add Role</button>&nbsp;
				<button type="button" class="btn btn-light" data-toggle="modal" data-target="#add_hero">Add Hero</button>

			</div>
		</div>
	</nav>

	<div class="container mt-3">
		<div class="row mt-3">

			<?php
			include 'koneksi.php';
			$query = "SELECT * FROM hero";
			$sql = mysqli_query($koneksi, $query);
			while($data = mysqli_fetch_array($sql)){

			echo '
			<div class="col-md-3 mb-3">
				<div class="card">
					<img class="card-img-top" src="images/'.$data['image'].'" style="width:100%">
					<div class="card-body">
						<h5 class="card-title">'  .$data["name"]. '</h5>
						<p class="card-text">'  .$data["deskripsi"]. '</p>
						<button type="button" class="btn btn-light" data-toggle="modal" data-target="#detail_hero">Details</button>
					</div>
				</div>
			</div>
			';
		}
		?>

	</div>

	<!-- Modal -->
	<div class="modal" id="add_hero">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Add Hero</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form method="post" action="add_hero.php" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nama : </label>
							<input type="text" class="form-control" name="name" required>
						</div>
						<?php
						$query = "SELECT * FROM role";
						$result = mysqli_query($koneksi, $query);
						?>
						<div class="form-group">
							<label>Role : </label>
							<select class="form-control" name="id_role">
								<?php while($data = mysqli_fetch_assoc($result) ){?>
								<option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label>Deskripsi : </label>
							<input type="textarea" class="form-control" name="deskripsi">
						</div>
						<div class="form-group">
							<label>Image : </label>
							<input type="file" class="form-control-file" name="image" required>
						</div>

						<input class="btn btn-success" type="submit" value="Simpan">
  
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal" id="add_role">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Add Role</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form method="post" action="add_role.php" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nama Role : </label>
							<input type="text" class="form-control" name="name" required>
						</div>

						<input class="btn btn-success" type="submit" value="Simpan">
  
					</form>
				</div>
			</div>
		</div>
	</div>

</body>
</html>